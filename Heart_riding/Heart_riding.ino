#include <LiquidCrystal.h>

LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

byte smile1[8] = {
B00000,
B00111,
B01111,
B11111,
B11111,
B11111,
B11111,
B01111,
};

byte smile2[8] = {
B00000,
B00000,
B10001,
B11011,
B11111,
B11111,
B11111,
B11111,
};

byte smile3[8] = {
B00000,
B11100,
B11110,
B11111,
B11111,
B11111,
B11111,
B11110,
};

byte smile4[8] = {
B01111,
B00111,
B00011,
B00001,
B00000,
B00000,
B00000,
B00000,
};

byte smile5[8] = {
B11111,
B11111,
B11111,
B11111,
B11111,
B11111,
B01110,
B00100,
};

byte smile6[8] = {
B11110,
B11100,
B11000,
B10000,
B00000,
B00000,
B00000,
B00000,
};

void setup (){
  lcd.createChar(1, smile1);
  lcd.createChar(2, smile2);
  lcd.createChar(3, smile3);
  lcd.createChar(4, smile4);
  lcd.createChar(5, smile5);
  lcd.createChar(6, smile6);
lcd.begin(16, 2);
lcd.setCursor (6, 0);
lcd.write(1);
lcd.write(2);
lcd.write(3);
lcd.setCursor(6, 1);
lcd.write(4);
lcd.write(5);
lcd.write(6);
delay(1000);
}
void loop(){
for (int i=0; i<9; i++){
lcd.scrollDisplayRight();
delay(200);
}
for (int i=0; i<17; i++){
lcd.scrollDisplayLeft();
delay(200);
}

for (int i=0; i<8; i++) {
lcd.scrollDisplayRight();
delay(200);
}
}
