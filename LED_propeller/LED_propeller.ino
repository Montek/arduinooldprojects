String textMsg="HELL";

byte LA[5]={0x7e, 0x9, 0x9, 0x9, 0x7e};
byte LB[5]={0x7f, 0x49, 0x49, 0x49, 0x36};
byte LC[5]={0x3e, 0x41, 0x41, 0x41, 0x22};
byte LD[5]={0x7f, 0x41, 0x41, 0x22, 0x1c};
byte LE[5]={0x7f, 0x49, 0x49, 0x49, 0x41};
byte LF[5]={0x7f, 0x9, 0x9, 0x9, 0x1};
byte LG[5]={0x3e, 0x41, 0x49, 0x49, 0x7a};
byte LH[5]={0x7f, 0x8, 0x8, 0x8, 0x7f};
byte LI[5]={0x0, 0x41, 0x7f, 0x41, 0x0};
byte LJ[5]={0x20, 0x40, 0x41, 0x3f, 0x1};
byte LK[5]={0x7f, 0x8, 0x14, 0x22, 0x41};
byte LL[5]={0x7f, 0x40, 0x40, 0x40, 0x40};
byte LM[5]={0x7f, 0x2, 0xc, 0x2, 0x7f};
byte LN[5]={0x7f, 0x4, 0x8, 0x10, 0x7f};
byte LO[5]={0x3e, 0x41, 0x41, 0x41, 0x3e};
byte LP[5]={0x7f, 0x9, 0x9, 0x9, 0x6};
byte LQ[5]={0x3e, 0x41, 0x51, 0x21, 0x5e};
byte LR[5]={0x7f, 0x9, 0x19, 0x29, 0x46};
byte LS[5]= {0x46, 0x49, 0x49, 0x49, 0x31};
byte LT[5]={0x1, 0x1, 0x7f, 0x1, 0x1};
byte LU[5]={0x3f, 0x40, 0x40, 0x40, 0x3f};
byte LV[5]={0x1f, 0x20, 0x40, 0x20, 0x1f};
byte LW[5]={0x3f, 0x40, 0x38, 0x40, 0x3f};
byte LX[5]={0x63, 0x14, 0x8, 0x14, 0x63};
byte LY[5]={0x3, 0x4, 0x78, 0x4, 0x3};
byte LZ[5]={0x61, 0x51, 0x49, 0x45, 0x43};
/*
byte plus[5]={0x8, 0x8, 0x3e, 0x8, 0x8};
byte minus[5]={0x8, 0x8, 0x8, 0x8, 0x8};
byte dot[5]={0x0, 0x60, 0x60, 0x0, 0x0};
byte comma[5]={0x0, 0x50, 0x30, 0x0, 0x0};
byte lbracket[5]={0x0, 0x1c, 0x22, 0x41, 0x0};
byte rbracket[5]={0x0, 0x41, 0x22, 0x1c, 0x0};
byte colon[5]= {0x0, 0x36, 0x36, 0x0, 0x0};

byte N0[5]={0x3e, 0x51, 0x49, 0x45, 0x3e};
byte N1[5]={0x0, 0x42, 0x7f, 0x40, 0x0};
byte N2[5]={0x42, 0x61, 0x51, 0x49, 0x46};
byte N3[5]={0x21, 0x41, 0x45, 0x4b, 0x31};
byte N4[5]={0x18, 0x14, 0x12, 0x7f, 0x10};
byte N5[5]={0x27, 0x45, 0x45, 0x45, 0x39};
byte N6[5]={0x3c, 0x4a, 0x49, 0x49, 0x30};
byte N7[5]={0x1, 0x1, 0x79, 0x5, 0x3};
byte N8[5]={0x36, 0x49, 0x49, 0x49, 0x36};
byte N9[5]={0x6, 0x49, 0x49, 0x29, 0x1e};
*/

byte space[1]={0x0};

int dataPin = 0;   //14 on shifter     //Define which pins will be used for the Shift Register control
int latchPin = 1;  //12 on shifter
int clockPin = 2;
int every[100];
int sign[5];
int textLength;
char givenSign;

void setup() { 
//Serial.begin(9600);
    pinMode(dataPin, OUTPUT);       //Configure each IO Pin
    pinMode(latchPin, OUTPUT);
    pinMode(clockPin, OUTPUT); 
    textLength = textMsg.length(); // find the length og the word
    int constant=1;
    
    for (int j=1; j<=textLength; j++){ 
    givenSign = textMsg.charAt(j); 
   
   switch (givenSign){
   case 'A':
   for (int k=0; k<5; k++){
   sign[k]=LA[k];
 }
    break;
   case 'B':
   for (int k=0; k<5; k++){
   sign[k]=LB[k];
 }
     break;
     case 'C':
   for (int k=0; k<5; k++){
   sign[k]=LC[k]; 
 }
     break;
   case 'D':
   for (int k=0; k<5; k++){
   sign[k]=LD[k];  
 }
     break;
     case 'E':
   for (int k=0; k<5; k++){
   sign[k]=LE[k];  
 }
     break;
   case 'F':
   for (int k=0; k<5; k++){
   sign[k]=LF[k]; 
 }
     break;
     case 'G':
   for (int k=0; k<5; k++){
   sign[k]=LG[k]; 
 }
     break;
   case 'H':
   for (int k=0; k<5; k++){
   sign[k]=LH[k]; 
 }
     break;
     case 'I':
   for (int k=0; k<5; k++){
   sign[k]=LI[k];  
 }
     break;
   case 'J':
   for (int k=0; k<5; k++){
   sign[k]=LJ[k]; 
 }
     break;
     case 'K':
   for (int k=0; k<5; k++){
   sign[k]=LK[k]; 
 }
     break;
   case 'L':
   for (int k=0; k<5; k++){
   sign[k]=LL[k];  
 }
     break;
     case 'M':
   for (int k=0; k<5; k++){
   sign[k]=LM[k];  
 }
     break;
   case 'N':
   for (int k=0; k<5; k++){
   sign[k]=LN[k];  
 }
     break;
     case 'O':
   for (int k=0; k<5; k++){
   sign[k]=LO[k];  
 }
     break;
   case 'P':
   for (int k=0; k<5; k++){
   sign[k]=LP[k];  
 }
     break;
     case 'Q':
   for (int k=0; k<5; k++){
   sign[k]=LQ[k];  
 }
     break;
   case 'R':
   for (int k=0; k<5; k++){
   sign[k]=LR[k];  
 }
     break;
     case 'S':
   for (int k=0; k<5; k++){
   sign[k]=LS[k];  
 }
     break;
   case 'T':
   for (int k=0; k<5; k++){
   sign[k]=LT[k];  
 }
     break;
     case 'U':
   for (int k=0; k<5; k++){
   sign[k]=LU[k];  
 }
     break;
   case 'V':
   for (int k=0; k<5; k++){
   sign[k]=LV[k];  
 }
     break;
    case 'W':
   for (int k=0; k<5; k++){
   sign[k]=LW[k];   
 }
     break;
   case 'X':
   for (int k=0; k<5; k++){
   sign[k]=LX[k];   
 }
     break;
     case 'Y':
   for (int k=0; k<5; k++){
   sign[k]=LY[k];   
 }
     break;
   case 'Z':
   for (int k=0; k<5; k++){
   sign[k]=LZ[k];   
 }
 
 /*
     break;
   case '0':
   for (int k=0; k<5; k++){
   sign[k]=N0[k];
     break;}
     case '1':
   for (int k=0; k<5; k++){
   sign[k]=N1[k];
 }
     break;
   case '2':
   for (int k=0; k<5; k++){
   sign[k]=N2[k]; 
 }
     break;
     case '3':
   for (int k=0; k<5; k++){
   sign[k]=N3[k];
 }
     break;
   case '4':
   for (int k=0; k<5; k++){
   sign[k]=N4[k];
   }
     break;
     case '5':
   for (int k=0; k<5; k++){
   sign[k]=N5[k];
   }  */
     break;
     default:;
   }
   
 for (int s = 0; s<5; s++){
   every[constant]=sign[s];
   constant++;
}
}
}

//*******************************************************
void loop() {
writting();
delayMicroseconds (300000);
}



//-------------------------------------------------------
void writting (){
   for (int i=1; i<=textLength*5; i++){
digitalWrite(latchPin, LOW);             
shiftOut(dataPin, clockPin, MSBFIRST, every[i]);     
digitalWrite(latchPin, HIGH);
delayMicroseconds(10);
digitalWrite(latchPin, LOW);             
shiftOut(dataPin, clockPin, MSBFIRST, space[1]);          
digitalWrite(latchPin, HIGH);
delayMicroseconds(20);

if (i%5==0){
delayMicroseconds (50);
} 

   }
} 
