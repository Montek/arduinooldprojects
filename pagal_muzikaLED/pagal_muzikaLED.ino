int maxVal = 700;
int minVal = 0;
const int ledCount = 6;
int ledPins [] = {2, 3, 4, 5, 6, 7};

void setup(){
for (int thisLed = 0; thisLed < ledCount; thisLed++)
pinMode (ledPins[thisLed], OUTPUT);
}

void loop(){
int sensorValue = analogRead(A0);
delay(30);
int val = map(sensorValue, minVal, maxVal, 0, ledCount);
for (int thisLed = 0; thisLed < ledCount; thisLed++){
if (val > thisLed){
 digitalWrite (ledPins[thisLed], HIGH);  
}  else {digitalWrite(ledPins[thisLed], LOW);}
}
delay(50);
}
