#include <IRremote.h>

//#define result1 0xBDA
#define result1 0x3C861BDA   //Codes that will be received
#define result2 0x3C861BF4
#define lightTime 5000   // defines the time for the light to be ON after not getting a signal

int RECV_PIN = 4;   //pin for IR data receiving
int pjezo = 8;    //pjezo element pin
unsigned long IRreceived = 0;  //Initial value, that will be changed. For storing codes
int led=5;   //led pin
IRrecv irrecv(RECV_PIN);   // makes variable RECV_PIN as a IR receiver pin
decode_results results;    // Data firtly be stored in variable results
int test = 6;     //this pin checks if a device must be turned into a working mode
unsigned long time=0;    //time to check if a signal was received or not
boolean a = false;   //variable for changing indicator's mode (ON/OFF)
boolean state1 = false; //just checks if state1 == state2, because of the button bouncing
boolean state2 = false;
boolean previousState= false;  //previous state must not be equal to state that is now to start operation


void setup()
{
  pinMode(test, OUTPUT);
   pinMode(led, OUTPUT);
   pinMode(pjezo, OUTPUT);
  Serial.begin(9600);
  irrecv.enableIRIn(); // Start the receiver
  
  for (int i = 0; i<13; i++){
  delay(100);
  a=~a;
  digitalWrite(test, a); //test indicates POWER ON mode
  }
  pinMode (A5, INPUT);  // input button. For turning on Data receiver
}

void loop() {
  if (irrecv.decode(&results)) {   //receives the code, stores into results
    IRreceived = results.value; //stores into IRreceived
    Serial.println (IRreceived, HEX);
    irrecv.resume(); // Receive the next value
  }  
  button();
  digitalWrite(test, a);
  if (a==false){
    IRreceived = 0;
  pause(); //no data receiving. The receiver is OFF
  }
  
  
   if (IRreceived == result2){ //Identifies correct code for pjezo element
   lights(pjezo, result2);
   }
   if (IRreceived == result1){  //correct for LEDs
   lights(led, result1);
   }
  IRreceived = 0; // Previously received value is 0
}
//-------------------------------------------------------
void lights(int pin, unsigned long operation){
 IRreceived = 0;
  time = millis(); //initial time
  while(1){ //never ending loop
    unsigned long currentMillis=millis();  // starts counting new time
  digitalWrite(pin, HIGH); 
 
     if (irrecv.decode(&results)) {
       IRreceived = results.value;
       Serial.println (IRreceived, HEX);
       irrecv.resume();
     }

  if (IRreceived == operation){   //operation == code of the result
  time = currentMillis; // time has new initial value "NOW"
  IRreceived = 0; // deletes information
  }    
  button();
  digitalWrite(test, a);
  if (a==false){
  digitalWrite(pin, LOW);
 IRreceived = 0;
 currentMillis=  lightTime + time + 10000;
  pause();
  }
  if (currentMillis-time > lightTime){
  digitalWrite(pin, LOW);
  break; //breaks loop while
}
}
}

void button(){
state1 = digitalRead(A5);
  delay(10);
  state2 = digitalRead(A5); 
  if (state1 == state2){      
  if (state1 != previousState){   //ignores bouncing
    if (state1 == LOW){
  a=~a;  
    }
  previousState=state1;
  }
  }
}
void pause(){ 
while (a == false){
button();
}
IRreceived = 0;
loop();
}
