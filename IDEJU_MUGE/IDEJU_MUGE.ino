#include <SPI.h>
#include <stdio.h>
#include <string.h>
#include <DS1302.h>
// http://www.arduino.cc/en/Tutorial/LiquidCrystal
// http://arduino.cc/playground/Main/LiquidCrystal
#include <LiquidCrystal.h>
#include <IRremote.h>

/*#define right  0x3C861BC2
#define left  0x3C861BCA
#define power  0x3C861BD2 */

#define firstOp  0x3C861BDA
#define secondOp 0x3C861BF4  

#define sendTime 1000
//-----------------------------------------------
uint8_t CE_PIN   = 5;  // for time chip
uint8_t IO_PIN   = 6;
uint8_t SCLK_PIN = 8;
/* Create buffers */
char buf[50];
char day[10];
/* Create a DS1302 object */
DS1302 rtc(CE_PIN, IO_PIN, SCLK_PIN);
int Hour, Minute, Day, Month, Year, Date, Second;
int lastTime;
//------------------------------------------------
unsigned long backTime = 0;
//int RECV_PIN = 4;         // for receiving IRDA
//unsigned long IRreceived = 0x00;
//IRrecv irrecv(RECV_PIN);  // for receiving IRDA
//decode_results results;
int led=7;            // LCD lights
LiquidCrystal lcd(10);  
boolean ON=false;
const char lg[]= "LIGHTS";
const char sn[]= "SOUND";
const char arr1[]= "<-----";
const char arr2[]= "---->";
int buttonState=0;
unsigned long time=0;
IRsend irsend;
int sound = 1;
int light = 2;
int function[10];         // only with buttons.....
int switchPin[10];          //

int right = 11;                //
int left = 12;                 //
int power = 14;                //
int operation = 13;            //
int backGround = 10;           // 
int extra = 15;                //

int IRreceived = 0;          //
boolean val, state, val2; // only with buttons.....
boolean timeMachine;

int alarmMinute, alarmHour;

void setup() {
  // Time t(2012, 11, 16, 19, 52, 00, 6);
rtc.write_protect(false);
rtc.halt(false);
  /* Make a new time object to set the date and time */
  /*   Tuesday, May 19, 2009 at 21:16:37.            */
 /* Set the time and date on the chip */
 //rtc.time(t);


timeMachine = true; 
  
 switchPin[1] = A0;
  switchPin[2] = A1;
   switchPin[3] = A2;
    switchPin[4] = A3;
     switchPin[5] = A4;
      switchPin[6] = A5; 
 
 function[1] = backGround;
  function[2] = right;
   function[3] = left;
    function[4] = operation;
     function[5] = power;
      function[6] = extra;
 
  for(int i=1; i<=6; i++){
  pinMode(switchPin[i], INPUT);
}

  pinMode(led, OUTPUT);
  lcd.begin(16, 2);  
  digitalWrite(led, HIGH);
//  irrecv.enableIRIn();       // for receiving IRDA
  Serial.begin(9600);
  introduction(); // begin time
}
//*************************************************
void loop() {
 /* if (irrecv.decode(&results)) {
   IRreceived = results.value;
   Serial.println (IRreceived, HEX);
   irrecv.resume(); // Receive the next value
  }
  */
   button();   
  Serial.print(IRreceived); 
  
  if (IRreceived == extra&& ON == false){
  alarm_clock();
  }
   if (IRreceived == backGround && ON == false){
   timeMachine = false;  
   backLights();} 
   if (IRreceived == power && ON == false){
   timeMachine = false;
   powerON();  // turn my arduino to a sender
   ON = true;
   }
   if (IRreceived == power && ON == true){
   ON = false;
   timeMachine = true;
   }
   if (timeMachine == true){
   introduction();
   }
   if (alarmMinute == Minute && alarmHour == Hour && Second == 0){
   powerON();
   }
   scrolling();
   IRreceived = 0;
}
//*************************************************

//------------------------------powerON----------------

void powerON()  { 
  lcd.clear(); 
  
  if(buttonState == sound){
  lcd.setCursor(5, 0);
   lcd.print(lg);
   lcd.setCursor(5, 1);
   lcd.print(arr1);
   lcd.setCursor(16, 0);   
   lcd.print(sn);
   lcd.setCursor(16, 1);
   lcd.print(arr2);  
  for (int i = 0; i < 11; i++) {
  lcd.scrollDisplayLeft(); 
} 
  } else {
   lcd.setCursor(5, 0);
   lcd.print(lg);
   lcd.setCursor(5, 1);
   lcd.print(arr1);
   lcd.setCursor(16, 0);   
   lcd.print(sn);
   lcd.setCursor(16, 1);
   lcd.print(arr2);
   buttonState = light;}  
   IRreceived = 0;
   
   if (alarmMinute == Minute && alarmHour == Hour && Second == 0){
   sending(firstOp);
   }
   
 }
 //--------------------------------introduction---------------------
 void introduction(){
  if (buttonState==sound){
  for (int i = 0; i < 11; i++) {
    lcd.scrollDisplayRight(); 
   buttonState = light; 
} 
}
    get_time();
if (Second != lastTime){
    lcd.setCursor(4,0);
    if (Hour <10){
    lcd.print ("0");
    }
    lcd.print(Hour);  
    lcd.print(":"); 
    if (Minute <10){
    lcd.print ("0");
    }
    lcd.print(Minute); 
    lcd.print(":");  
    
    if (Second < 10){
    lcd.print ("0");
    }
  
    lcd.print(Second); 
    lcd.setCursor(0, 1);
    lcd.print (" ");
    lcd.print(day);
    lcd.print(" "); 
    lcd.print(Year); 
    lcd.print("-");
   if (Month <10){
    lcd.print ("0");
    } 
    lcd.print(Month); 
    lcd.print("-"); 
    if (Date <10){
    lcd.print ("0");
    }
    lcd.print(Date); 
 lastTime = Second;
 }
 }
 //--------------------------------scrolling--------------------
 void scrolling (){
   
 if(buttonState == sound && IRreceived == left )
   IRreceived = 0; 
   if(buttonState == light && IRreceived == right )
   IRreceived = 0;  
   
   if (IRreceived == left && ON == true){
   buttonState = sound; 
   for (int i = 0; i < 11; i++) {
   lcd.scrollDisplayLeft(); 
   delay(100);
  } 
  
  }
  if(IRreceived == right && ON == true){
  buttonState = light;
  for (int i = 0; i < 11; i++) {
    lcd.scrollDisplayRight(); 
    delay(100);
} 
} 
   if (IRreceived == operation && buttonState == light && ON == true){ 
   sending(firstOp);
   }
   if (IRreceived == operation && buttonState == sound && ON == true){ 
   sending(secondOp);
   }
 }
//-------------------------sending------------------
void sending(unsigned long name ){
  IRreceived=0;
  time = 0;
  while(1){
    unsigned long currentMillis=millis();
    button();
    
  if (IRreceived == operation)
{
  for (int i=1; i<=2; i++){
 lcd.noDisplay();
 delay(300);
 lcd.display();
 delay(300);}
  break;
}

 if (currentMillis - time > sendTime){
 lcd.noDisplay();
 for (int i=0; i<2; i++){
    irsend.sendSony(name, 32); // Sony TV power code
    delay(100);
}
    time = currentMillis + 200;
 lcd.display();
}
}
}
//-----------------------------inserting a button--------------

void button (){
  for (int i = 1; i<=6; i++){
  val = digitalRead(switchPin[i]);  
  delay(10);
  val2 = digitalRead(switchPin[i]);
  if (val == val2){
  if (val != state){
  if (val == HIGH) {
  IRreceived = function[i];
  while (digitalRead(switchPin[i])==HIGH){}
  }}
  state = val;
  }}}

//-----------------------------backlights-------------------------------
void backLights(){
IRreceived = 0;
while(IRreceived != backGround){
button();
get_time();
digitalWrite (led, LOW);
lcd.noDisplay();
if (alarmMinute == Minute && alarmHour == Hour && Second == 0){
IRreceived = backGround;
}
}
digitalWrite(led, HIGH);
lcd.display();
timeMachine = true;
}

//-------------------------------------------------------------

void get_time(){
  /* Get the current time and date from the chip */
  Time t = rtc.time();
  /* Name the day of the week */
  memset(day, 0, sizeof(day));  /* clear day buffer */
  switch (t.day) {
    case 1:
      strcpy(day, "Sun");
      break;
    case 2:
      strcpy(day, "Mon");
      break;
    case 3:
      strcpy(day, "Tue");
      break;
    case 4:
      strcpy(day, "Wed");
      break;
    case 5:
      strcpy(day, "Thu");
      break;
    case 6:
      strcpy(day, "Fri");
      break;
    case 7:
      strcpy(day, "Sat");
      break;
  }
  /* Format the time and date and insert into the temporary buffer */
 /* snprintf(buf, sizeof(buf), "%s %04d-%02d-%02d %02d:%02d:%02d",
           day,
           t.yr, t.mon, t.date,
           t.hr, t.min, t.sec);*/
Year = t.yr;
Month = t.mon;
Date = t.date;
Hour = t.hr;
Minute = t.min;
Second = t.sec; 
  /* Print the formatted string to serial so we can see the time */
 // Serial.println(buf);
}

void alarm_clock(){
IRreceived=0;
lcd.clear();
while (IRreceived != extra){  
button(); 
if (IRreceived == right){
alarmMinute++;
if (alarmMinute == 60){
alarmMinute = 0;
}
IRreceived=0;
}
if (IRreceived == left){
alarmMinute--;
if (alarmMinute == -1){
alarmMinute = 59;
}
IRreceived=0;
}
if (IRreceived == power){
alarmHour--;
if (alarmHour == -1){
alarmHour = 23;
}
IRreceived=0;
}
if (IRreceived == operation){
alarmHour++;
if (alarmHour == 24){
alarmHour = 0;
}
IRreceived=0;
}
lcd.setCursor(6,0);
if (alarmHour < 10){
lcd.print ("0");
}
lcd.print(alarmHour);
lcd.print(":");
if (alarmMinute < 10){
lcd.print ("0");
}
lcd.print(alarmMinute);
}
lcd.clear();
}

