#include <LiquidCrystal.h>

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

int thisChar = 'a';
int number = 0;
void setup() {
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  // turn on the cursor:
  lcd.cursor();
}

void loop() {
  // print the character
  lcd.home();
  lcd.write(thisChar);
 
  if (thisChar == 'm' && number == 0){
    lcd.setCursor(3,1);
    lcd.write(thisChar);
    thisChar = 'a';
    number++;
  }
  if (thisChar == 'o'&& number == 1) {
  lcd.setCursor(4,1);
    lcd.write(thisChar);
    thisChar = 'a';
     number++;
  }
  if (thisChar == 'n' && number == 2)
  {
    lcd.setCursor(5,1);
    lcd.write(thisChar);
    thisChar = 'a';
     number++;
  }
  if (thisChar == 't' && number == 3)
  {
    lcd.setCursor(6,1);
    lcd.write(thisChar);
    thisChar = 'a';
     number++;
  }
  if (thisChar == 'e' && number == 4)
  {
    lcd.setCursor(7,1);
    lcd.write(thisChar);
    thisChar = 'a';
     number++;
  }
  // increment the letter:
  delay(100);
  thisChar++;
}
