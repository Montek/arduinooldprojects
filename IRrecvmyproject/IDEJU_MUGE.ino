#include <SPI.h>
// http://www.arduino.cc/en/Tutorial/LiquidCrystal
// http://arduino.cc/playground/Main/LiquidCrystal
// include the library code:
#include <LiquidCrystal.h>
#include <IRremote.h>
// initialize the library with the number of the sspin 
//(or the latch pin of the 74HC595)
int RECV_PIN = 3;
unsigned long IRreceived = 0x00;
LiquidCrystal lcd(10);
IRrecv irrecv(RECV_PIN);
decode_results results;
unsigned int lightTime = 5000;
int buttonState=0;
unsigned long right = 0x3C861BC2;
unsigned long left = 0x3C861BCA;
unsigned long power = 0x3C861BD2;
unsigned long operation = 0x3C861BDA;
int led = 6;
unsigned long time=0;
int pjezo = 5;

void setup() {
  pinMode(led, OUTPUT);
  // set up the LCD's number of columns and rows: 
  lcd.begin(16, 2);
  // Print a message to the LCD.
  irrecv.enableIRIn();
  Serial.begin(9600);
    lcd.setCursor(4,0);
    lcd.print("PRESS ON");
}

void loop() {
  if (irrecv.decode(&results)) {
   IRreceived = results.value;
   Serial.println (IRreceived, HEX);
    irrecv.resume(); // Receive the next value
  }
   if (IRreceived == power)
   powerON();
   if(buttonState == 1 && IRreceived == left )
   IRreceived = 0; 
   if(buttonState == 2 && IRreceived == right )
   IRreceived = 0;
   
   if (IRreceived == left){
   buttonState = 1; 
   for (int i = 0; i < 11; i++) {
   lcd.scrollDisplayLeft(); 
   delay(200);
  } 
  }
  if(IRreceived == right){
  buttonState = 2;
  for (int i = 0; i < 11; i++) {
    lcd.scrollDisplayRight(); 
    delay(200);
} 
} 
   if (IRreceived == operation && buttonState ==1){ 
   light(led);
   }
   if (IRreceived == operation && buttonState ==2){ 
   light(pjezo);
   }
   
  IRreceived = 0; 
}



//--------------------------------------------
void light(int led){
 IRreceived = 0;
  time = millis(); 
  while(1){ 
unsigned long currentMillis=millis();
  digitalWrite(led, HIGH);  
 
 if (irrecv.decode(&results)) {
   IRreceived = results.value;
   Serial.println (IRreceived, HEX);
   irrecv.resume();
  } 
  if (IRreceived == operation){
  time = currentMillis;
  IRreceived = 0;
  }    

  if (currentMillis-time > lightTime){
  digitalWrite(led, LOW);
  break;}
  }
}
//----------------------------------------------

//----------------------------------------------
void powerON()  { 
   lcd.clear();  
   lcd.setCursor(16, 0);
   lcd.print("LIGHTS");
   lcd.setCursor(16, 1);
   lcd.print("----->");
   for (int i = 0; i < 10; i++){
   lcd.scrollDisplayLeft(); 
   delay(200);
  }
   lcd.setCursor(5, 0);   
   lcd.print("SOUND");
   lcd.setCursor(5, 1);
   lcd.print("<----");
   buttonState = 1;
   IRreceived = 0;
 }
