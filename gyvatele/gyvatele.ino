#include <glcd.h>
#include <gldc_Buildinfo.h>
#include <gldc_Config.h>
#include <bitmaps/pv_band1.h>
#include <fonts/allFonts.h>

int W = GLCD.Width;
int H = GLCD.Height;
int k = 0;
int buttonPins[2] = {2, 3};
const int STORIS = 10;
const int AUKSTIS = 15;
const int riba = -10;
boolean game_over;
int points;
// raidziu ekrane telpa 21x8

struct rect {
  int x;
  int y;
  int w;
  int h;
  
  boolean check(rect arg) {
    return !((x > arg.x + arg.w) || (x + w < arg.x) || (y + h < arg.y) || (y > arg.y + arg.h));
  }
  
  void ini(int X, int Y, int width, int height) {
    x = X;
    y = Y;
    w = width;
    h = height;
  }
  
  void update(int deltaX) {
    x -= deltaX;
  }
  
  void draw() {
    if (x > riba && x < 128)
      GLCD.FillRect(max(x, 0), y, w - 1 + min(x, 0), h - 1);
  }
};

struct rutuliukas {
  int y;
  int velocity;
  rect r;
  
  void ini() {
    y = H/2;
    velocity = 5;
    r.ini(3, y, 4, 4);
  }
  
  void update() {
  
    if (!digitalRead(buttonPins[0]))
      y -= velocity;
    else if (!digitalRead(buttonPins[1]))
      y += velocity;
    
    r.y = y;
    
    if (y < 0)
      y = 0;
    else if (y > 60)
      y = 60;
    
    Serial.println(y);
  }
  
  void draw() {
    GLCD.FillRect(3, y, 3, 3);
  }
  
};

struct siena {
  rect a, b;
  int x;
  int velocity;
  
  void ini(int X, int Y) {
    a.ini(X, 0, STORIS, Y);
    b.ini(X, Y + AUKSTIS, STORIS, H - Y - AUKSTIS);
    x = X;
    velocity = 4;
  }
  
  void update() {
    x -= velocity;
    a.update(velocity);
    b.update(velocity);
  }
  
  void draw() {
    a.draw();
    b.draw();
  }
};

rutuliukas R;

struct sienos {
  siena S[10];
  
  int MIN_ATSTUMAS;
  int MAX_ATSTUMAS;
  int lastX;
  
  void update() {
    S[0].update();
    lastX = S[0].x;
    
    for (int i = 1;i < 10;i++) {
      S[i].update();
      lastX = max(lastX, S[i].x); 
    }
    
    Serial.write("S[] = {");
    for (int i = 0;i < 10;i++) {
      Serial.print(S[i].x);
      Serial.write(", ");
    }
    Serial.println("}");
    
    Serial.println(lastX);
    
    for (int i = 0;i < 10;i++) {
      if (S[i].x < riba) {
        Serial.println("pridedu");
        points++;
        S[i].ini(lastX + random(MIN_ATSTUMAS, MAX_ATSTUMAS), random(10, H - AUKSTIS - 10));
        lastX = S[i].x;
      }
    }
  }
  
  void ini() {
    
    MIN_ATSTUMAS = 30;
    MAX_ATSTUMAS = 80;
    
    for (int i = 0;i < 10;i++)
      S[i].x = riba - 10;
    lastX = 130;
    
    for (int i = 0;i < 10;i++) {
      if (S[i].x < riba) {
        S[i].ini(lastX + random(MIN_ATSTUMAS, MAX_ATSTUMAS), random(10, H - AUKSTIS - 10));
        lastX = S[i].x;
      }
    }
  }
  
  void draw() {
    for (int i = 0;i < 10;i++)
      S[i].draw();
  }
};

sienos S;


void setup() {
  Serial.begin(9600);
  Serial.println("Setup start");
  
  GLCD.Init();
  GLCD.SelectFont(System5x7);
  
  points = 0;
  game_over = false;
  
  R.ini();
  R.draw();
  
  S.ini();
  S.draw();
  
  for (int i = 0;i < 2;i++)
    pinMode(buttonPins[i], INPUT);
  
  randomSeed(analogRead(5));
  
  Serial.println("Setup end");
}

void loop() {
  GLCD.ClearScreen();
  
  if (game_over == false) {
    R.update();
    R.draw();
    S.draw();
    S.update();
  } else {
    GLCD.CursorTo(9, 3);
    GLCD.print("GAME OVER");
    
    for (int i = 3;i > 0;i--) {
      GLCD.CursorTo(7,3);
      GLCD.print(i);
      delay(1000);
    }
    asm volatile ("jmp 0");
  } 
  
  GLCD.CursorTo(10,1);
  GLCD.print(points);
  Serial.write("Taskai - ");
  Serial.println(points);
  
  for (int i = 0;i < 10;i++) {
    if (R.r.check(S.S[i].a)) {
      game_over = true;
    }
    if (R.r.check(S.S[i].b)) {
      game_over = true;
    }
  }
  
  delay(50);
  
  for (int i = 0;i < 2;i++)
    if (!digitalRead(buttonPins[i]))
      Serial.println(i);
  
}
