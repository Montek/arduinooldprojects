int dataPin = 11;        //Define which pins will be used for the Shift Register control
int latchPin = 8;
int clockPin = 12;

int seq[14] = {1,2,4,8,16,32,64,128,64,32,16,8,4,2};       //The byte sequence

void setup()
{
    pinMode(dataPin, OUTPUT);       //Configure each IO Pin
    pinMode(latchPin, OUTPUT);
    pinMode(clockPin, OUTPUT);
}

void loop()
{
    for (int n = 0; n < 14; n++)
    {
        digitalWrite(latchPin, LOW);             //Pull latch LOW to start sending data
        shiftOut(dataPin, clockPin, MSBFIRST, seq[n]);          //Send the data
        digitalWrite(latchPin, HIGH);            //Pull latch HIGH to stop sending data
        delay(75);
    }
}
