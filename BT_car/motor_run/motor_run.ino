int forw = 3;
int back = 4;
int motor_speed = 5;

void setPwmFrequency(int pin, int divisor) {
  byte mode;
  if(pin == 5 || pin == 6 || pin == 9 || pin == 10) {
    switch(divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 64: mode = 0x03; break;
      case 256: mode = 0x04; break;
      case 1024: mode = 0x05; break;
      default: return;
    }
    if(pin == 5 || pin == 6) {
      TCCR0B = TCCR0B & 0b11111000 | mode;
    } else {
      TCCR1B = TCCR1B & 0b11111000 | mode;
    }
  } else if(pin == 3 || pin == 11) {
    switch(divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 32: mode = 0x03; break;
      case 64: mode = 0x04; break;
      case 128: mode = 0x05; break;
      case 256: mode = 0x06; break;
      case 1024: mode = 0x7; break;
      default: return;
    }
    TCCR2B = TCCR2B & 0b11111000 | mode;
  }
}

void run (int spd, int turning)
{
  if (turning == 1)
  {
    digitalWrite (forw, 1);
    digitalWrite (back, 0);
  } else 
  {
    digitalWrite (forw, 0);
    digitalWrite (back, 1);
  }
  analogWrite (motor_speed, spd*2.55);
}

void setup()
{
  setPwmFrequency(5, 8);
  pinMode (forw, OUTPUT);
  pinMode (back, OUTPUT);
  
}

void loop()
{
  int i;
  int a = 5;
  
  for (i = 0; i<100; i+=a)
  {
    run (i, 1);
    delay (1000);
  }
  
  delay (8000);
  
  for (i; i>-1; i-=a)
  {
    run (i, 1);
    delay (1000);
  }
  
  delay (8000);
}
