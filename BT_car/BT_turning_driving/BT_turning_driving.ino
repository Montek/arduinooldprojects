#include <SoftwareSerial.h>// import the serial library
#include <Servo.h> 


String voice;

SoftwareSerial BT(10, 11); // RX, TX
Servo myservo;

int forw = 3;
int back = 4;
int motor_speed = 5;
//****************************PROGRAMS**********************
void run (int spd, int turning);
//*****************************SETUP************************
void setup()
{
 // setPwmFrequency(5, 8);
  pinMode (forw, OUTPUT);
  pinMode (back, OUTPUT);
  
  myservo.attach(9);
  myservo.write (90);
  
  Serial.begin(9600);
  BT.begin(9600);
}
//*****************************LOOP*************************
void loop()
{
  if (BT.available())
  {
    delay(5);
    char c = BT.read();
    Serial.println (c);
    
    if (c == 'S')
    {
      myservo.write(90);
      run (0, 1);
      delay(5);
    }
    
    if (c == 'F')
    {
      run(70, 1);
      myservo.write (90);
      delay(5);
    }
    
    if(c == 'B')
    {
       run(70, -1); 
       myservo.write (90);
       delay(5);
    }

    if (c == 'L')
    {
      myservo.write(70);
      delay(5);
    }
    
    if (c == 'R')
    {
      myservo.write(120);
      delay(5);
    }
    
    if (c == 'I')
    {
      myservo.write(120);
      run (70, 1);
      delay(5);
    }
    
    if (c == 'G')
    {
      myservo.write(70);
      run (70, 1);
      delay(5);
    }
   
    if (c == 'H')
    {
      myservo.write(70);
      run (70, -1);
      delay(5);
    }
    
    if (c == 'J')
    {
      myservo.write(120);
      run (70, -1);
      delay(5);
    }
  }
}
//*********************************END**********************

void run (int spd, int turning)
{
  if (turning == 1)
  {
    digitalWrite (forw, 1);
    digitalWrite (back, 0);
  } 
  if (turning == -1)
  {
    digitalWrite (forw, 0);
    digitalWrite (back, 1);
  }
  analogWrite (motor_speed, spd*2.55);
}









void setPwmFrequency(int pin, int divisor) {
  byte mode;
  if(pin == 5 || pin == 6 || pin == 9 || pin == 10) {
    switch(divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 64: mode = 0x03; break;
      case 256: mode = 0x04; break;
      case 1024: mode = 0x05; break;
      default: return;
    }
    if(pin == 5 || pin == 6) {
      TCCR0B = TCCR0B & 0b11111000 | mode;
    } else {
      TCCR1B = TCCR1B & 0b11111000 | mode;
    }
  } else if(pin == 3 || pin == 11) {
    switch(divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 32: mode = 0x03; break;
      case 64: mode = 0x04; break;
      case 128: mode = 0x05; break;
      case 256: mode = 0x06; break;
      case 1024: mode = 0x7; break;
      default: return;
    }
    TCCR2B = TCCR2B & 0b11111000 | mode;
  }
}
