#include <SoftwareSerial.h>// import the serial library

String voice;
SoftwareSerial BT(10, 11); // RX, TX

void setup() {
  Serial.begin(9600);
  BT.begin(9600);
}

void loop() {
  if (BT.available())
  {
    delay (10);
   
    int value2 = BT.read();
         
    Serial.println (value2, DEC);
  }
  delay(10);
}
