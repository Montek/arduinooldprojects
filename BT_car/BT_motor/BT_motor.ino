#include <SoftwareSerial.h>// import the serial library

String voice;
SoftwareSerial BT(10, 11); // RX, TX

int forw = 3;
int back = 4;
int motor_speed = 5;
//****************************PROGRAMS**********************
void run (int spd, int turning);
//*****************************SETUP************************
void setup()
{
 // setPwmFrequency(5, 8);
  pinMode (forw, OUTPUT);
  pinMode (back, OUTPUT);
  Serial.begin(9600);
  BT.begin(9600);
}
//*****************************LOOP*************************
void loop()
{
  if (BT.available())
  {
    delay(10);
    char c = BT.read();
    Serial.println (c);
    if (c == 'A')
    {
      run(90, 1);
      delay(1000);
    }
    run(0, 1);
    if(c == 'B')
    {
       run(90, -1); 
       delay(1000);
    }
    run(0,-1);
  }
}
//*********************************END**********************

void run (int spd, int turning)
{
  if (turning == 1)
  {
    digitalWrite (forw, 1);
    digitalWrite (back, 0);
  } 
  if (turning == -1)
  {
    digitalWrite (forw, 0);
    digitalWrite (back, 1);
  }
  analogWrite (motor_speed, spd*2.55);
}









void setPwmFrequency(int pin, int divisor) {
  byte mode;
  if(pin == 5 || pin == 6 || pin == 9 || pin == 10) {
    switch(divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 64: mode = 0x03; break;
      case 256: mode = 0x04; break;
      case 1024: mode = 0x05; break;
      default: return;
    }
    if(pin == 5 || pin == 6) {
      TCCR0B = TCCR0B & 0b11111000 | mode;
    } else {
      TCCR1B = TCCR1B & 0b11111000 | mode;
    }
  } else if(pin == 3 || pin == 11) {
    switch(divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 32: mode = 0x03; break;
      case 64: mode = 0x04; break;
      case 128: mode = 0x05; break;
      case 256: mode = 0x06; break;
      case 1024: mode = 0x7; break;
      default: return;
    }
    TCCR2B = TCCR2B & 0b11111000 | mode;
  }
}
