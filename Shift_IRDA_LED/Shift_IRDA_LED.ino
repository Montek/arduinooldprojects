#include <IRremote.h>

int val;
int val2;
int state;
int buttonState = 0;
int b=0x1;
int a = 0x0;
long prevMillis;
long interval = 100;

int RECV_PIN = 3;

unsigned long IRreceived = 0x00;
IRrecv irrecv(RECV_PIN);
decode_results results;

int dataPin = 11;   //14 on shifter     //Define which pins will be used for the Shift Register control
int latchPin = 8;  //12 on shifter
int clockPin = 12;

void setup()
{
    pinMode(dataPin, OUTPUT);       //Configure each IO Pin
    pinMode(latchPin, OUTPUT);
    pinMode(clockPin, OUTPUT);
    irrecv.enableIRIn();
}

void loop(){
    if (irrecv.decode(&results)) {
   IRreceived = results.value;
    irrecv.resume(); // Receive the next value
  } 
if (IRreceived == 0x3C861BD2)
 buttonState = 0;
if (IRreceived == 0x3C861BF2)
 buttonState = 1;
if(IRreceived == 0x3C861BFA)
 buttonState = 2;
if(IRreceived == 0x3C861BDA)
 buttonState = 3; 
//----------------------------------------------
 if (buttonState == 1)
digitalWrite(latchPin, LOW);             
shiftOut(dataPin, clockPin, MSBFIRST, 0xFF);          
digitalWrite(latchPin, HIGH);
//----------------------------------------------
if (buttonState == 2){
unsigned long currentMillis = millis();
if ( currentMillis - prevMillis >= interval) {
prevMillis = currentMillis;
digitalWrite(latchPin, LOW);            
shiftOut(dataPin, clockPin, MSBFIRST, a);        
digitalWrite(latchPin, HIGH);     
a = ~a;
}
}
//--------------------------------
if (buttonState == 3) {
digitalWrite(latchPin, LOW);            
shiftOut(dataPin, clockPin, MSBFIRST, b);        
digitalWrite(latchPin, HIGH);     
b = b<<1;
delay(50);
if (b > 0x80)
b=0x1;
}
if (buttonState ==0){
digitalWrite(latchPin, LOW);            
shiftOut(dataPin, clockPin, MSBFIRST, 0x0);        
digitalWrite(latchPin, HIGH);     
b=0x1;
a=0x0;
}
}
