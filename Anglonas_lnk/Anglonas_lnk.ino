/*
Example sketch for interfacing with the DS1302 timekeeping chip.

Copyright (c) 2009, Matt Sparks
All rights reserved.

http://quadpoint.org/projects/arduino-ds1302
*/
#include <stdio.h>
#include <string.h>
#include <DS1302.h>

/* Set the appropriate digital I/O pin connections */
uint8_t CE_PIN   = 5;
uint8_t IO_PIN   = 6;
uint8_t SCLK_PIN = 8;
int lastTime, Second, Hour, Minute, Day, Month, Year, Date;

int alh, almin;
int hour, minutes;
/* Create buffers */
char buf[50];
char day[10];

/* Create a DS1302 object */
DS1302 rtc(CE_PIN, IO_PIN, SCLK_PIN);


void print_time()
{
  /* Get the current time and date from the chip */
  Time t = rtc.time();

  /* Name the day of the week */
  memset(day, 0, sizeof(day));  /* clear day buffer */
  switch (t.day) {
    case 1:
      strcpy(day, "Sunday");
      break;
    case 2:
      strcpy(day, "Monday");
      break;
    case 3:
      strcpy(day, "Tuesday");
      break;
    case 4:
      strcpy(day, "Wednesday");
      break;
    case 5:
      strcpy(day, "Thursday");
      break;
    case 6:
      strcpy(day, "Friday");
      break;
    case 7:
      strcpy(day, "Saturday");
      break;
  }

  /* Format the time and date and insert into the temporary buffer */
Second = t.sec;  
if (Second != lastTime){  
  snprintf(buf, sizeof(buf), "%s %04d-%02d-%02d %02d:%02d:%02d",
           day,
           t.yr, t.mon, t.date,
           t.hr, t.min, t.sec);
hour = t.hr;
minutes = t.min;
  /* Print the formatted string to serial so we can see the time */
  Serial.println(buf);
lastTime = t.sec;
}
}


void setup()
{
  Serial.begin(9600);

  /* Initialize a new chip by turning off write protection and clearing the
     clock halt flag. These methods needn't always be called. See the DS1302
     datasheet for details. */
 rtc.write_protect(false);
 rtc.halt(false);

  /* Make a new time object to set the date and time */
  /*   Tuesday, May 19, 2009 at 21:16:37.            */
Time t(2012, 11, 16, 18, 20, 00, 6);

  /* Set the time and date on the chip */
  rtc.time(t);
  alh = 20;
  almin = 52;
}


/* Loop and print the time every second */
void loop()
{
  if (alh == hour && almin == minutes)
  Serial.println("Alarm is ON!");
  print_time();
  delay(1000);
}


void get_time(){
  /* Get the current time and date from the chip */
  Time t = rtc.time();
  /* Name the day of the week */
  memset(day, 0, sizeof(day));  /* clear day buffer */
  switch (t.day) {
    case 1:
      strcpy(day, "Sun");
      break;
    case 2:
      strcpy(day, "Mon");
      break;
    case 3:
      strcpy(day, "Tue");
      break;
    case 4:
      strcpy(day, "Wed");
      break;
    case 5:
      strcpy(day, "Thu");
      break;
    case 6:
      strcpy(day, "Fri");
      break;
    case 7:
      strcpy(day, "Sat");
      break;
  }
  /* Format the time and date and insert into the temporary buffer */
 /* snprintf(buf, sizeof(buf), "%s %04d-%02d-%02d %02d:%02d:%02d",
           day,
           t.yr, t.mon, t.date,
           t.hr, t.min, t.sec);*/
Year = t.yr;
Month = t.mon;
Date = t.date;
Hour = t.hr;
Minute = t.min;
Second = t.sec; 
  /* Print the formatted string to serial so we can see the time */
 // Serial.println(buf);
}

 void introduction(){
   get_time();
   if (Second != lastTime){
    lcd.setCursor(4,0);
    Serial.print(Hour);  
    lcd.print(":"); 
    lcd.print(Minute); 
    lcd.print(":"); 
    lcd.print(Second); 
    lcd.setCursor(0, 1);
    lcd.print(day);
    lcd.print(" "); 
    lcd.print(Year); 
    lcd.print("/"); 
    lcd.print(Month); 
    lcd.print("/"); 
    lcd.print(Date); 
 lastTime = Second;
 }
 }
