const int sensorPin = A0; int sensorValue = 0;         // the sensor value
int sensorMin = 1023;        // minimum sensor value
int sensorMax = 0; 

void setup() {
  Serial.begin(9600);
   pinMode(13, OUTPUT);
  digitalWrite(13, HIGH);

  // calibrate during the first five seconds
  while (millis() < 10000) {
    sensorValue = analogRead(sensorPin);

    // record the maximum sensor value
    if (sensorValue > sensorMax) {
      sensorMax = sensorValue;
    }

    // record the minimum sensor value
    if (sensorValue < sensorMin) {
      sensorMin = sensorValue;
    }
  }
  // signal the end of the calibration period
  digitalWrite(13, LOW);
}

void loop(){
digitalWrite(13, LOW);
Serial.print ("min = ");
Serial.println (sensorMin);
Serial.print ("max = ");
Serial. println (sensorMax);
delay(10000);
}
